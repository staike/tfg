package com.babuindev.bog.tea;

import org.junit.Test;
import static org.junit.Assert.*;

public class ActivityTeaTest {

    @Test
    public void parseURLisCorrect(){
        TeaActivity tea = new TeaActivity();
        assertEquals("http://uah.es", tea.parseToCorrectURL("http://uah.es"));
    }
}
