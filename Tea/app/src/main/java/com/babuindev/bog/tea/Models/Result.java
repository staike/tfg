package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @SerializedName("elementos")
    private Elements elements;
    @SerializedName("imagen")
    private String image;
    @SerializedName("nivel")
    private String level;
    @SerializedName("principios")
    private List<ValuesResult> valuesResultList;
    @SerializedName("resolucion")
    private String resolution;
    @SerializedName("resumen")
    private Summary summary;
    @SerializedName("url")
    private String url;

    public Result(Elements elements, String image, String level, List<ValuesResult> valuesResultList,
                  String resolution, Summary summary, String url) {
        this.elements = elements;
        this.image = image;
        this.level = level;
        this.valuesResultList = valuesResultList;
        this.resolution = resolution;
        this.summary = summary;
        this.url = url;
    }

    public Elements getElements() {
        return elements;
    }

    public void setElements(Elements elements) {
        this.elements = elements;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public List<ValuesResult> getValuesResultList() {
        return valuesResultList;
    }

    public void setValuesResultList(List<ValuesResult> valuesResultList) {
        this.valuesResultList = valuesResultList;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
