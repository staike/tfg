package com.babuindev.bog.tea;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.widget.TextView;

import com.babuindev.bog.tea.Common.Session;
import com.babuindev.bog.tea.Models.Techniques;

public class DetailsActivity extends AppCompatActivity {

    private TextView titleTv;
    private TextView codeTv;
    private TextView clasificationTv;
    private TextView criticityTv;
    private TextView observationsTv;
    private TextView recomendationsTv;

    //Technique object with all the information of this activity.
    private Techniques technique;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        //We show the back button on the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getViews();

        setInfo();

    }

    public void getViews(){

        titleTv = findViewById(R.id.details_title_tv);
        codeTv = findViewById(R.id.code_details_tv);
        clasificationTv = findViewById(R.id.clasification_details_tv);
        criticityTv = findViewById(R.id.criticity_details_tv);
        observationsTv = findViewById(R.id.observations_details_tv);
        recomendationsTv = findViewById(R.id.recomendation_detailss_tv);

    }


    public void setInfo(){
        //Get the selected techniques from session.
        technique = Session.getInstance(getApplicationContext()).getTechniques();
        //Set texts
        titleTv.setText(technique.getTitle());
        codeTv.setText(getString(R.string.details_code)+" "+technique.getCode());
        clasificationTv.setText(getString(R.string.details_state)+" "+technique.getClasification());
        criticityTv.setText(getString(R.string.details_criticity)+" "+technique.getCriticiti());
        String observations = technique.getObservations();
        observations = replaceXMPCode(observations,"<img","&lt;img");
        observations = replaceXMPCode(observations,"<a", "&lt;a");
        observationsTv.setText(convertFromHTMLFormat(observations));
        recomendationsTv.setText(convertFromHTMLFormat(technique.getRecomendation()));
    }

    /**
     * Method to format the HTML text that came from the API to actual good formated text
     * @param inputText
     * @return
     */
    public Spanned convertFromHTMLFormat(String inputText){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return(Html.fromHtml(inputText, Html.FROM_HTML_MODE_COMPACT));
        } else {
            return (Html.fromHtml(inputText));
        }
    }

    /**
     * Method to replace img and a markups with &lt so converter don't interpret it
     * @param base
     * @param oldReference
     * @param newReference
     * @return
     */
    public String replaceXMPCode(String base, String oldReference, String newReference){
        return base.replace(oldReference,newReference);
    }


}
