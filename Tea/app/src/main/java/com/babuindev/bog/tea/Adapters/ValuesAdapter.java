package com.babuindev.bog.tea.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.babuindev.bog.tea.Common.Session;
import com.babuindev.bog.tea.Interface.ValuesAdapterInterface;
import com.babuindev.bog.tea.Models.Guidelines;
import com.babuindev.bog.tea.R;

import java.util.List;

public class ValuesAdapter extends RecyclerView.Adapter<ValuesAdapter.CardViewHolder> {

    List<Guidelines> guidelinesList;
    ValuesAdapterInterface listener;

    public ValuesAdapter(List<Guidelines> guidelinesList, ValuesAdapterInterface listener){
        this.guidelinesList = guidelinesList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ValuesAdapter.CardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view,viewGroup,false);
        CardViewHolder cardViewHolder = new CardViewHolder(v);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ValuesAdapter.CardViewHolder cardViewHolder, int i) {
        cardViewHolder.title.setText(guidelinesList.get(i).getNumber()+" "+guidelinesList.get(i).getTitle());
        cardViewHolder.description.setText(guidelinesList.get(i).getDescription());
        final int b = i;
        cardViewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.isClick(guidelinesList.get(b));
            }
        });

    }

    @Override
    public int getItemCount() {
        if(guidelinesList!=null){
            return guidelinesList.size();
        }else{
            return 0;
        }
    }


    //View Holder for adapter
    public static class CardViewHolder extends RecyclerView.ViewHolder{

        CardView cv;
        TextView title;
        TextView description;
        Button button;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            title = itemView.findViewById(R.id.cv_title);
            description = itemView.findViewById(R.id.cv_description);
            button = itemView.findViewById(R.id.cv_button);
        }
    }
}
