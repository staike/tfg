package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.SerializedName;


public class Elements{

    @SerializedName("formularios")
    private int forms;
    @SerializedName("iframes")
    private int iframes;
    @SerializedName("imagenes")
    private int images;
    @SerializedName("links")
    private int links;
    @SerializedName("linksImagen")
    private int linksImage;
    @SerializedName("objects")
    private int objects;
    @SerializedName("tablas")
    private int tables;
    @SerializedName("total")
    private int total;

    public Elements(int forms, int iframes, int images, int links, int linksImage, int objects, int tables, int total) {
        this.forms = forms;
        this.iframes = iframes;
        this.images = images;
        this.links = links;
        this.linksImage = linksImage;
        this.objects = objects;
        this.tables = tables;
        this.total = total;
    }

    public int getForms() {
        return forms;
    }

    public void setForms(int forms) {
        this.forms = forms;
    }

    public int getIframes() {
        return iframes;
    }

    public void setIframes(int iframes) {
        this.iframes = iframes;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public int getLinks() {
        return links;
    }

    public void setLinks(int links) {
        this.links = links;
    }

    public int getLinksImage() {
        return linksImage;
    }

    public void setLinksImage(int linksImage) {
        this.linksImage = linksImage;
    }

    public int getObjects() {
        return objects;
    }

    public void setObjects(int objects) {
        this.objects = objects;
    }

    public int getTables() {
        return tables;
    }

    public void setTables(int tables) {
        this.tables = tables;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
