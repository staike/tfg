package com.babuindev.bog.tea.Network;

import com.babuindev.bog.tea.Models.DownloadResult;
import com.babuindev.bog.tea.Models.OAW;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterface {

    //Methods to get JSON data
    
    @GET("conformidad/")
    Call<DownloadResult> getAnalisis(@Query("url") String url, @Query("key") String apiKey,
                                     @Query("nivel") String conformity, @Query("resolucion") String resolution);

    //Method to get JSON in play text, not gson parsed.
    @GET("conformidad/")
    Call<ResponseBody> downloadAnalisis(@Query("url") String url, @Query("key") String apiKey,
                                        @Query("nivel") String conformity, @Query("resolucion") String resolution);
}
