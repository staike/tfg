package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.SerializedName;


public class DownloadResult{

    @SerializedName("oaw")
    private OAW oaw;

    public DownloadResult(OAW oaw) {
        this.oaw = oaw;
    }

    public OAW getOaw() {
        return oaw;
    }

    public void setOaw(OAW oaw) {
        this.oaw = oaw;
    }
}
