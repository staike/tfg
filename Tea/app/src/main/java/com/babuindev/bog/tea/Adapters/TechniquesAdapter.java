package com.babuindev.bog.tea.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.babuindev.bog.tea.Interface.TechniquesAdapterInterface;
import com.babuindev.bog.tea.Models.Techniques;
import com.babuindev.bog.tea.R;

import java.util.List;

public class TechniquesAdapter extends RecyclerView.Adapter<TechniquesAdapter.CardViewHolder> {

    private Context context;
    private List<Techniques> techniquesList;
    private TechniquesAdapterInterface listener;

    public TechniquesAdapter(Context context, List<Techniques> techniquesList, TechniquesAdapterInterface listener){
        this.context = context;
        this.techniquesList = techniquesList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view,viewGroup,false);
        CardViewHolder cardViewHolder = new CardViewHolder(view);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder cardViewHolder, int i) {
        cardViewHolder.title.setText(techniquesList.get(i).getTitle());
        cardViewHolder.details.setText(techniquesList.get(i).getCode()+" - "+
                context.getString(R.string.card_criticity)+" "+techniquesList.get(i).getCriticiti());
        final int b = i;
        cardViewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.isClick(techniquesList.get(b));
            }
        });
    }


    @Override
    public int getItemCount() {
        if(techniquesList!=null){
            return techniquesList.size();
        }else{
            return 0;
        }
    }

    public void setTechniquesList(List<Techniques> techniquesList) {
        this.techniquesList = techniquesList;
        this.notifyDataSetChanged();
    }

    class CardViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView details;
        Button button;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.cv_title);
            details = itemView.findViewById(R.id.cv_description);
            button = itemView.findViewById(R.id.cv_button);
        }
    }
}
