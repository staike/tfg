package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.babuindev.bog.tea.Adapters.AlwaysListTypeAdapterFactory;

import java.util.Arrays;
import java.util.List;

public class Guidelines{
    @SerializedName("numero")
    private String number;
    @SerializedName("descripcion")
    private String description;
    @SerializedName("titulo")
    private String title;
    @SerializedName("criterios")
    @JsonAdapter(AlwaysListTypeAdapterFactory.class)
    private List<Criteria> criteria;

    public Guidelines(String number, String description, String title, List<Criteria> criteria) {
        this.number = number;
        this.description = description;
        this.title = title;
        this.criteria = criteria;
    }

    public Guidelines() {

    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Criteria> getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria ... ms) {
        criteria = Arrays.asList(ms);
    }
}
