package com.babuindev.bog.tea;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.babuindev.bog.tea.Adapters.CriteriaAdapter;
import com.babuindev.bog.tea.Common.Session;
import com.babuindev.bog.tea.Interface.CriteriaAdapterInterface;
import com.babuindev.bog.tea.Models.Criteria;
import com.babuindev.bog.tea.Models.Guidelines;

public class CriteriaActivity extends AppCompatActivity {
    private Guidelines sessionGuidelines;
    private RecyclerView rv;
    private CriteriaAdapterInterface listener;
    private CriteriaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criteria);

        //We show the back button on the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getViews();

        setListeners();

        setInfo();

    }

    public void getViews(){
        rv = findViewById(R.id.rv_criteria);

    }

    public void setListeners(){
        //Interface listener implementation
        listener = new CriteriaAdapterInterface() {
            @Override
            public void isClick(Criteria criteria) {
                //Set to session the criteria that was clicked.
                Session.getInstance(getApplicationContext()).setCriteria(criteria);
                Intent intent = new Intent(CriteriaActivity.this,TechniquesActivity.class);
                startActivity(intent);

            }
        };

    }

    public void setInfo(){
        sessionGuidelines = Session.getInstance(getApplicationContext()).getGuidelines();
        setRecycleView();
    }

    public void setRecycleView(){
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        //Create new adapter from criteria list.
        adapter = new CriteriaAdapter(sessionGuidelines.getCriteria(),listener);
        //Set adapter to recycleView.
        rv.setAdapter(adapter);
    }
}
