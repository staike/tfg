package com.babuindev.bog.tea.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.babuindev.bog.tea.Interface.CriteriaAdapterInterface;
import com.babuindev.bog.tea.Models.Criteria;
import com.babuindev.bog.tea.R;

import java.util.List;

public class CriteriaAdapter extends RecyclerView.Adapter<CriteriaAdapter.CardViewHolder> {

    List<Criteria> criteriaList;
    CriteriaAdapterInterface listener;

    public CriteriaAdapter(List<Criteria> guidelinesList, CriteriaAdapterInterface listener){
        this.criteriaList = guidelinesList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CriteriaAdapter.CardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view,viewGroup,false);
        CardViewHolder cardViewHolder = new CardViewHolder(v);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CriteriaAdapter.CardViewHolder cardViewHolder, int i) {
        cardViewHolder.title.setText(criteriaList.get(i).getNumber()+" "+criteriaList.get(i).getTitle());
        cardViewHolder.description.setText(criteriaList.get(i).getLevel());
        final int b = i;
        cardViewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.isClick(criteriaList.get(b));
            }
        });

    }

    @Override
    public int getItemCount() {
        if(criteriaList!=null){
            return criteriaList.size();
        }else{
            return 0;
        }
    }


    //View Holder for adapter
    public static class CardViewHolder extends RecyclerView.ViewHolder{

        CardView cv;
        TextView title;
        TextView description;
        Button button;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            title = itemView.findViewById(R.id.cv_title);
            description = itemView.findViewById(R.id.cv_description);
            button = itemView.findViewById(R.id.cv_button);
        }
    }
}
