package com.babuindev.bog.tea.Models;

import com.babuindev.bog.tea.Adapters.AlwaysListTypeAdapterFactory;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ValuesResult {

    @SerializedName("numero")
    private int number;
    @SerializedName("titulo")
    private String titulo;
    @SerializedName("descripcion")
    private String description;
    @JsonAdapter(AlwaysListTypeAdapterFactory.class)
    @SerializedName("pautas")
    private List<Guidelines> guidelinesList;

    public ValuesResult(int number, String titulo, String description, List<Guidelines> guidelinesList) {
        this.number = number;
        this.titulo = titulo;
        this.description = description;
        this.guidelinesList = guidelinesList;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Guidelines> getGuidelinesList() {
        return guidelinesList;
    }

    public void setGuidelinesList(List<Guidelines> guidelinesList) {
        this.guidelinesList = guidelinesList;
    }
}
