package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.SerializedName;


public class Techniques {

    @SerializedName("codigo")
    private String code;
    @SerializedName("criticidad")
    private String criticiti;
    @SerializedName("titulo")
    private String title;
    @SerializedName("exitos")
    private int succes;
    @SerializedName("errores")
    private int errors;
    @SerializedName("observacion")
    private String observations;
    @SerializedName("recomendacion")
    private String recomendation;
    @SerializedName("clasificacion")
    private String clasification;

    public Techniques(String code, String criticiti, String title, int succes, int errors, String observations,
                      String recomendation, String clasification) {
        this.code = code;
        this.criticiti = criticiti;
        this.title = title;
        this.succes = succes;
        this.errors = errors;
        this.observations = observations;
        this.recomendation = recomendation;
        this.clasification = clasification;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCriticiti() {
        return criticiti;
    }

    public void setCriticiti(String criticiti) {
        this.criticiti = criticiti;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSucces() {
        return succes;
    }

    public void setSucces(int succes) {
        this.succes = succes;
    }

    public int getErrors() {
        return errors;
    }

    public void setErrors(int errors) {
        this.errors = errors;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getRecomendation() {
        return recomendation;
    }

    public void setRecomendation(String recomendation) {
        this.recomendation = recomendation;
    }

    public String getClasification() {
        return clasification;
    }

    public void setClasification(String clasification) {
        this.clasification = clasification;
    }
}
