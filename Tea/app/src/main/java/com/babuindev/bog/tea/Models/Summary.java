package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.SerializedName;


public class Summary {

    @SerializedName("comprensible")
    private SummaryComprensible comprensible;
    @SerializedName("operable")
    private SummaryOperable operable;
    @SerializedName("perceptible")
    private SummaryPerceptible perceptible;
    @SerializedName("robusto")
    private SummaryRobust robust;
    @SerializedName("totalComprensible")
    private String totalComprensible;
    @SerializedName("totalOperable")
    private String totalOperable;
    @SerializedName("totalPerceptible")
    private String totalPerceptible;
    @SerializedName("totalRobusto")
    private String totalRobuts;

    public Summary(SummaryComprensible comprensible, SummaryOperable operable, SummaryPerceptible perceptible,
                   SummaryRobust robust, String totalComprensible, String totalOperable, String totalPerceptible, String totalRobuts) {
        this.comprensible = comprensible;
        this.operable = operable;
        this.perceptible = perceptible;
        this.robust = robust;
        this.totalComprensible = totalComprensible;
        this.totalOperable = totalOperable;
        this.totalPerceptible = totalPerceptible;
        this.totalRobuts = totalRobuts;
    }

    public SummaryComprensible getComprensible() {
        return comprensible;
    }

    public void setComprensible(SummaryComprensible comprensible) {
        this.comprensible = comprensible;
    }

    public SummaryOperable getOperable() {
        return operable;
    }

    public void setOperable(SummaryOperable operable) {
        this.operable = operable;
    }

    public SummaryPerceptible getPerceptible() {
        return perceptible;
    }

    public void setPerceptible(SummaryPerceptible perceptible) {
        this.perceptible = perceptible;
    }

    public SummaryRobust getRobust() {
        return robust;
    }

    public void setRobust(SummaryRobust robust) {
        this.robust = robust;
    }

    public String getTotalComprensible() {
        return totalComprensible;
    }

    public void setTotalComprensible(String totalComprensible) {
        this.totalComprensible = totalComprensible;
    }

    public String getTotalOperable() {
        return totalOperable;
    }

    public void setTotalOperable(String totalOperable) {
        this.totalOperable = totalOperable;
    }

    public String getTotalPerceptible() {
        return totalPerceptible;
    }

    public void setTotalPerceptible(String totalPerceptible) {
        this.totalPerceptible = totalPerceptible;
    }

    public String getTotalRobuts() {
        return totalRobuts;
    }

    public void setTotalRobuts(String totalRobuts) {
        this.totalRobuts = totalRobuts;
    }


}
