package com.babuindev.bog.tea.Common;

import android.content.Context;

import com.babuindev.bog.tea.Models.Criteria;
import com.babuindev.bog.tea.Models.Guidelines;
import com.babuindev.bog.tea.Models.OAW;
import com.babuindev.bog.tea.Models.Techniques;
import com.babuindev.bog.tea.Models.ValuesResult;

public class Session {
    private static Session instance;
    private final Context context;
    //OAW object
    private OAW oaw;

    //All objects used in the OAW
    private ValuesResult perceptible;
    private ValuesResult operable;
    private ValuesResult comprensible;
    private ValuesResult robust;

    //Pauta
    private Guidelines guidelines;
    //Criterio
    private Criteria criteria;
    //Tecnica
    private Techniques techniques;

    //Variable to know wich valuesResult we have selected.
    private String valuesResultSelection;



    public static Session getInstance (Context context){
        if (instance == null) {
            context = context.getApplicationContext();
            instance = new Session(context);
        }
        return instance;
    }

    private Session (Context context){
        this.context = context;
    }


    public OAW getOaw() {
        return oaw;
    }

    public void setOaw(OAW oaw) {
        this.oaw = oaw;
    }

    public Context getContext(){
        return context;
    }

    public ValuesResult getPerceptible() {
        return perceptible;
    }

    public void setPerceptible(ValuesResult perceptible) {
        this.perceptible = perceptible;
    }

    public ValuesResult getOperable() {
        return operable;
    }

    public void setOperable(ValuesResult operable) {
        this.operable = operable;
    }

    public ValuesResult getComprensible() {
        return comprensible;
    }

    public void setComprensible(ValuesResult comprensible) {
        this.comprensible = comprensible;
    }

    public ValuesResult getRobust() {
        return robust;
    }

    public void setRobust(ValuesResult robust) {
        this.robust = robust;
    }

    public Guidelines getGuidelines() {
        return guidelines;
    }

    public void setGuidelines(Guidelines guidelines) {
        this.guidelines = guidelines;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public Techniques getTechniques() {
        return techniques;
    }

    public void setTechniques(Techniques techniques) {
        this.techniques = techniques;
    }

    public String getValuesResultSelection() {
        return valuesResultSelection;
    }

    public void setValuesResultSelection(String valuesResultSelection) {
        this.valuesResultSelection = valuesResultSelection;
    }
}
