package com.babuindev.bog.tea;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.babuindev.bog.tea.Common.Session;
import com.babuindev.bog.tea.Controllers.DataController;
import com.babuindev.bog.tea.Interface.TeaInterface;
import com.babuindev.bog.tea.Models.DownloadResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.ResponseBody;

public class TeaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextInputLayout urlLayoutTil;
    private TextInputEditText urlTiet;
    private Spinner resolutionSpinner;
    private Spinner conformitySpinner;
    private Button analizeButton;
    private Button downloadButton;
    private DataController dataController;
    private AlertDialog loadinDialog;
    private AlertDialog errorDialog;
    private AlertDialog downloadDialog;
    private AlertDialog finishDownloadingDialog;
    private EditText downloadName;

    private static final int INDEX_TEA_RESOLUTION_SPINNER = 1;
    private static final int INDEX_TEA_CONFORMITY_SPINNER = 2;
    private static final String API_KEY = "a58945bd-8477-4b36-aa19-94993f71cb7f";
    private static final int SERVER_ERROR = 0;
    private static final int CONECTION_ERROR = SERVER_ERROR + 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tea);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Get Views, set listeners and set info
        getViews();

        setListeners();

        setInfo();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.drawer_contact_button) {
            //Intent for Contact Activity
            Intent intent = new Intent(TeaActivity.this,ContactActivity.class);
            //We start the intent
            startActivity(intent);
        } else if (id == R.id.drawer_faq_button) {
            //Intent for FAQ Activity
            Intent intent = new Intent(TeaActivity.this,FAQActivity.class);
            //We start the intent
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    //Method to get all views
    public void getViews(){
        urlLayoutTil = findViewById(R.id.layout_url_til);
        urlTiet = findViewById(R.id.url_tiet);
        resolutionSpinner = findViewById(R.id.resolution_spinner);
        conformitySpinner = findViewById(R.id.conformity_spinner);
        analizeButton = findViewById(R.id.analize_button);
        downloadButton = findViewById(R.id.download_button);
    }

    //Method to set all listeners
    public void setListeners(){

        //Analize button listener.
        analizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadinDialog.show();
                dataController.getApiAnalisis(parseToCorrectURL(urlTiet.getText().toString()),
                        API_KEY, conformitySpinner.getSelectedItem().toString(),
                        resolutionSpinner.getSelectedItem().toString(), new TeaInterface.getAnalisisData() {
                    @Override
                    public void success(DownloadResult data) {
                        loadinDialog.dismiss();
                        //Add the OWA data to sessión Singleton.
                        Session.getInstance(getApplicationContext()).setOaw(data.getOaw());
                        //Make an intent and start the resume Activity
                        Intent intent = new Intent(TeaActivity.this,ResumeActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void notSucces(ResponseBody errorResponse) {
                            //Dismiss loading dialog if there is an error
                            loadinDialog.dismiss();
                            //Configure error dialog and show it
                            setErrorDialog(SERVER_ERROR);
                            errorDialog.show();
                    }

                    @Override
                    public void error(Throwable throwable) {
                        //Dismiss loading message if there is an error
                        loadinDialog.dismiss();
                        //Configure error conection dialog and show it
                        setErrorDialog(CONECTION_ERROR);
                        errorDialog.show();
                    }
                });
            }
        });

        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadDialog.show();
            }
        });
    }

    //Method to set all information
    public void setInfo(){
        //Set index of sinners.
        resolutionSpinner.setSelection(INDEX_TEA_RESOLUTION_SPINNER);
        conformitySpinner.setSelection(INDEX_TEA_CONFORMITY_SPINNER);

        dataController = new DataController();

        //Set loading dialog configuration
        setLoadingDialog();
        setDownloadDialog();
        setFinisDownloadingDialog();


    }

    /**
     * Method to configure the loading dialog
     */
    public void setLoadingDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(getApplicationContext()).inflate(R.layout.loading_dialog_layout,
                (ViewGroup) findViewById(android.R.id.content), false);
        //Set view of the alert dialog.
        alertDialogBuilder.setView(viewInflated);
        alertDialogBuilder.setCancelable(false);
        loadinDialog = alertDialogBuilder.create();
    }

    public void setFinisDownloadingDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle(getResources().getString(R.string.download_finished_title));
        alertDialogBuilder.setMessage(getResources().getString(R.string.download_finished));
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finishDownloadingDialog.dismiss();
            }
        });
        finishDownloadingDialog = alertDialogBuilder.create();
    }

    public void setDownloadDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        View viewInflated = LayoutInflater.from(getApplicationContext()).inflate(R.layout.download_popup_dialog,
                (ViewGroup) findViewById(android.R.id.content), false);
        //get edittext from the popup
        downloadName = viewInflated.findViewById(R.id.download_json_edittext);
        //Set view of the alert dialog.
        alertDialogBuilder.setView(viewInflated);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadinDialog.show();
                dataController.downloadApiAnalisis(parseToCorrectURL(urlTiet.getText().toString()),
                        API_KEY, conformitySpinner.getSelectedItem().toString(),
                        resolutionSpinner.getSelectedItem().toString(), new TeaInterface.downloadAnalisisData(){

                            @Override
                            public void success(ResponseBody data) {
                                try {
                                   // writeJSONtoStorage(downloadName.getText().toString(),data.string());
                                    writeJSONtoStorage(downloadName.getText().toString(),data.string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                downloadDialog.dismiss();
                                loadinDialog.dismiss();
                                //Show message that shows download is finished.
                                finishDownloadingDialog.show();

                            }

                            @Override
                            public void notSucces(ResponseBody errorResponse) {
                                //Dismiss loading dialog if there is an error
                                loadinDialog.dismiss();
                                //Configure error dialog and show it
                                setErrorDialog(SERVER_ERROR);
                                errorDialog.show();
                            }

                            @Override
                            public void error(Throwable throwable) {
                                //Dismiss loading message if there is an error
                                loadinDialog.dismiss();
                                //Configure error conection dialog and show it
                                setErrorDialog(CONECTION_ERROR);
                                errorDialog.show();
                            }
                        });
            }
        });
        alertDialogBuilder.setNegativeButton("Cancell", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                downloadDialog.dismiss();
            }
        });
        //We create the dialog from builder.
        downloadDialog = alertDialogBuilder.create();


    }

    /**
     * Method to configure the error dialog
     */
    public void setErrorDialog(int errorTipe){
        AlertDialog.Builder errorDialogBuilder;
        switch (errorTipe){
            case SERVER_ERROR:
                errorDialogBuilder = new AlertDialog.Builder(this);
                errorDialogBuilder.setTitle(getString(R.string.error_dialog_title));
                errorDialogBuilder.setMessage(getString(R.string.error_dialog_description));
                errorDialogBuilder.setPositiveButton(getString(R.string.error_dialog_accept_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                //We generate the alert dialog from builder
                errorDialog = errorDialogBuilder.create();
                break;
            case CONECTION_ERROR:
                errorDialogBuilder = new AlertDialog.Builder(this);
                errorDialogBuilder.setTitle(getString(R.string.error_dialog_title));
                errorDialogBuilder.setMessage(getString(R.string.error_dialog_description));
                errorDialogBuilder.setPositiveButton(getString(R.string.error_dialog_accept_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                //We generate the alert dialog from builder
                errorDialog = errorDialogBuilder.create();
                break;

                default:
                    errorDialogBuilder = new AlertDialog.Builder(this);
                    errorDialogBuilder.setTitle(getString(R.string.error_dialog_title));
                    errorDialogBuilder.setMessage(getString(R.string.error_dialog_description));
                    errorDialogBuilder.setPositiveButton(getString(R.string.error_dialog_accept_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    //We generate the alert dialog from builder
                    errorDialog = errorDialogBuilder.create();
                    break;
        }

    }

    /**
     * Method that receive an URL and parse it to a valid URL
     * @return
     */
    public String parseToCorrectURL(String url){
        String finalUrl=url;
        String prefix_http = "";
        String prefix_https = "";

        if(url.length()>4){
            prefix_http = url.substring(0,4);
            prefix_https = url.substring(0,5);

            if(!prefix_https.equals("https")){
                if(!prefix_http.equals("http")){
                    finalUrl = "http://"+url;
                }
            }
        }

        return finalUrl;
    }

    /**
     * Method to download the json to internal file.
     */
    public void writeJSONtoStorage(String name, String text){
        File path = this.getExternalFilesDir(null);
        File file = new File(path, name+".json");
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            stream.write(text.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }




}
