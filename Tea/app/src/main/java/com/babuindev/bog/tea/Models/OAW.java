package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.SerializedName;


public class OAW {

    @SerializedName("fecha")
    private String date;
    @SerializedName("resultado")
    private Result result;

    public OAW(String date, Result result) {
        this.date = date;
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
