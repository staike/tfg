package com.babuindev.bog.tea;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.babuindev.bog.tea.Common.Constants;
import com.babuindev.bog.tea.Common.Session;
import com.babuindev.bog.tea.Models.OAW;
import com.babuindev.bog.tea.Models.ValuesResult;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

public class ResumeActivity extends AppCompatActivity {

    private RadarChart chart;
    private Button perceptibleButton;
    private Button operableButton;
    private Button comprensibleButton;
    private Button robustButton;
    private ArrayList<RadarEntry> chartEntryes;
    private OAW oawResult;
    private Session session;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume);

        //We show the back button on the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getViews();

        setListeners();

        setInfo();


    }

    //Get all views
    public void getViews(){
        chart = findViewById(R.id.radar_chart_resume);
        perceptibleButton = findViewById(R.id.resume_perceptible_button);
        operableButton = findViewById(R.id.resume_operable_button);
        comprensibleButton = findViewById(R.id.resume_comprensible_button);
        robustButton = findViewById(R.id.resume_robust_button);
    }

    //Set listeners for buttons
    public void setListeners(){
        perceptibleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValueSelectedAndIntent(Constants.ResumeActivityPrincipes.PERCEPTIBLE);
            }
        });

        operableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValueSelectedAndIntent(Constants.ResumeActivityPrincipes.OPERABLE);
            }
        });

        comprensibleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValueSelectedAndIntent(Constants.ResumeActivityPrincipes.COMPRENSIBLE);
            }
        });

        robustButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValueSelectedAndIntent(Constants.ResumeActivityPrincipes.ROBUSTO);
            }
        });
    }

    //Set all info
    public void setInfo(){
        session = Session.getInstance(getApplicationContext());
        oawResult = session.getOaw();
        //Set chart
        setEntriesAndChart();
        //Set the respective valuesResult (Perceptible, Operable, Comprensible and Robust) to session so we can use them.
        setValuesResult();
    }

    /**
     * Method to set entries of chart and chart configuration
     */
    public void setEntriesAndChart(){
        chartEntryes = new ArrayList<>();
        //Set chart data
        chartEntryes.add(new RadarEntry(Float.parseFloat(oawResult.getResult().getSummary()
                .getTotalPerceptible().replace("%",""))));
        chartEntryes.add(new RadarEntry(Float.parseFloat(oawResult.getResult().getSummary()
                .getTotalRobuts().replace("%",""))));
        chartEntryes.add(new RadarEntry(Float.parseFloat(oawResult.getResult().getSummary()
                .getTotalComprensible().replace("%",""))));
        chartEntryes.add(new RadarEntry(Float.parseFloat(oawResult.getResult().getSummary()
                .getTotalOperable().replace("%",""))));

        ArrayList<String> labels = new ArrayList<>();
        labels.add(getString(R.string.resume_chart_perceptible));
        labels.add(getString(R.string.resume_chart_robust));
        labels.add(getString(R.string.resume_chart_comprensible));
        labels.add(getString(R.string.resume_chart_operable));


        RadarDataSet dataSet = new RadarDataSet(chartEntryes,Constants.CHART_NAME);
        //Set color of dataset graph
        dataSet.setColor(Color.CYAN);
        //Fill with color the draw area
        dataSet.setDrawFilled(true);

        //Radar data formed by the dataset.
        RadarData data = new RadarData(dataSet);
        //Give format to the XAxis so labels are showed.
        XAxis xAxis = chart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));

        //Get the YAxis and set values, so the minimum value drawed of the chart is 0f, the max is 60f wich correspond to 100
        //and also set granularity to go from 20 to 20 units.
        YAxis yAxis = chart.getYAxis();
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(60f);
        yAxis.setGranularity(20f);

        //Quit legend and make the chart not touchable
        chart.getLegend().setEnabled(false);
        chart.setTouchEnabled(false);

        //Set data to chart.
        chart.setData(data);
        chart.invalidate();
        chart.animate();

    }

    /**
     * Method that set ValuesResult to session singleton.
     */
    public void setValuesResult(){
        ArrayList<ValuesResult> arrayValuesResult = new ArrayList<>(oawResult.getResult().getValuesResultList());
        //Iterator
        for(ValuesResult value : arrayValuesResult){
            switch (value.getTitulo()){
                case Constants.ResumeActivityPrincipes.PERCEPTIBLE:
                    session.setPerceptible(value);
                    break;
                case Constants.ResumeActivityPrincipes.OPERABLE:
                    session.setOperable(value);
                    break;
                case Constants.ResumeActivityPrincipes.COMPRENSIBLE:
                    session.setComprensible(value);
                    break;
                case Constants.ResumeActivityPrincipes.ROBUSTO:
                    session.setRobust(value);
                    break;
            }
        }

    }

    /**
     * Method to set the value selected to session and after change to next Activity
     * @param valueSelected
     */
    public void setValueSelectedAndIntent(String valueSelected){
        session.setValuesResultSelection(valueSelected);
        Intent intent = new Intent(ResumeActivity.this,ValueActivity.class);
        startActivity(intent);

    }


}
