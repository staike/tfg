package com.babuindev.bog.tea.Common;

public final class Constants {

    public static final String CHART_NAME="Resumen";

    //RESUME ACTIVITY VALUE RESULT SWITCH
    public interface ResumeActivityPrincipes{
        String PERCEPTIBLE="Perceptible";
        String OPERABLE="Operable";
        String COMPRENSIBLE="Comprensible";
        String ROBUSTO="Robusto";
    }

    public interface TechniquesClasificationInterface{
        String SUCCESS = "Exito";
        String ERROR = "Error";
        String WARNING = "Advertencia";
        String NOTAPLY = "No aplica";
    }



}
