package com.babuindev.bog.tea.Interface;

import com.babuindev.bog.tea.Models.DownloadResult;

import okhttp3.ResponseBody;

public final class TeaInterface {

    public interface getAnalisisData{
        void success(DownloadResult data);
        void notSucces(ResponseBody errorResponse);
        void error(Throwable throwable);
    }

    public interface downloadAnalisisData{
        void success(ResponseBody data);
        void notSucces(ResponseBody errorResponse);
        void error(Throwable throwable);
    }

}
