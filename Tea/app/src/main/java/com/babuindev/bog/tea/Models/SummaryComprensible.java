package com.babuindev.bog.tea.Models;

import com.google.gson.annotations.SerializedName;


public class SummaryComprensible {

    @SerializedName("advertencias")
    private int warnings;
    @SerializedName("errores")
    private int errors;
    @SerializedName("exitos")
    private int success;
    @SerializedName("noaplica")
    private int notAplicable;

    public SummaryComprensible(int warnings, int errors, int success, int notAplicable) {
        this.warnings = warnings;
        this.errors = errors;
        this.success = success;
        this.notAplicable = notAplicable;
    }

    public int getWarnings() {
        return warnings;
    }

    public void setWarnings(int warnings) {
        this.warnings = warnings;
    }

    public int getErrors() {
        return errors;
    }

    public void setErrors(int errors) {
        this.errors = errors;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getNotAplicable() {
        return notAplicable;
    }

    public void setNotAplicable(int notAplicable) {
        this.notAplicable = notAplicable;
    }
}
