package com.babuindev.bog.tea.Controllers;

import com.babuindev.bog.tea.Interface.TeaInterface;
import com.babuindev.bog.tea.Models.DownloadResult;
import com.babuindev.bog.tea.Network.RetrofitInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataController {
    private static final String BASE_URL = "http://observatorioweb.ups.edu.ec/oaw/srv/wcag/json/";
    private RetrofitInterface service;

    public DataController(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .build();

        //GSON
        GsonBuilder gsonBuilder = new GsonBuilder();
        //Indicate to gsonBuilder to serialize null values
        //gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();

        //Get the retrofit service.
        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(RetrofitInterface.class);
    }

    //Method that perform the api call
    public void getApiAnalisis(String url, String api_key, final String level, final String resolution, final TeaInterface.getAnalisisData listener){
        Call<DownloadResult> getData = service.getAnalisis(url,api_key,level,resolution);
        getData.enqueue(new Callback<DownloadResult>() {
            @Override
            public void onResponse(Call<DownloadResult> call, Response<DownloadResult> response) {
                if(response.isSuccessful()){
                    listener.success(response.body());
                }else{
                    listener.notSucces(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<DownloadResult> call, Throwable t) {
                listener.error(t);
            }
        });
    }

    /**
     * Method to perform api call downloading result in play text.
     */
    public void downloadApiAnalisis(String url, String api_key, final String level, final String resolution, final TeaInterface.downloadAnalisisData listener){
        Call<ResponseBody> getData = service.downloadAnalisis(url,api_key,level,resolution);
        getData.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    listener.success(response.body());
                }else{
                    listener.notSucces(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.error(t);
            }
        });
    }
}
