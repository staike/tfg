package com.babuindev.bog.tea;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.babuindev.bog.tea.Adapters.ValuesAdapter;
import com.babuindev.bog.tea.Common.Constants;
import com.babuindev.bog.tea.Common.Session;
import com.babuindev.bog.tea.Interface.ValuesAdapterInterface;
import com.babuindev.bog.tea.Models.Guidelines;

public class ValueActivity extends AppCompatActivity {

    private Session session;
    private String valueSelected;
    private RecyclerView recyclerView;
    private ValuesAdapterInterface listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_value);

        //We show the back button on the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getViews();

        setListeners();

        setInfo();
    }

    public void getViews(){
        recyclerView = findViewById(R.id.rv_activity_value);
    }

    public void setListeners(){
        listener = new ValuesAdapterInterface() {
            @Override
            public void isClick(Guidelines guidelines) {
                Session.getInstance(getApplicationContext()).setGuidelines(guidelines);
                Intent intent = new Intent(ValueActivity.this,CriteriaActivity.class);
                startActivity(intent);
                //Toast.makeText(getApplicationContext(),Session.getInstance(getApplicationContext()).getValuesResultSelection(),Toast.LENGTH_LONG).show();
            }
        };
    }

    public void setInfo(){
        session = Session.getInstance(getApplicationContext());
        valueSelected = session.getValuesResultSelection();
        //Set the title from the value user selected in ResumeActivity
        this.setTitle(valueSelected);
        //Set all the code concerning recycleview
        setRecyclerView();

    }

    public void setRecyclerView(){
        //Set fixed size to the recycle view
        recyclerView.setHasFixedSize(true);
        //Add linear layout manager
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        //False initialization
        ValuesAdapter adapter = new ValuesAdapter(null,listener);
        switch (valueSelected){
            case Constants.ResumeActivityPrincipes.PERCEPTIBLE:
                adapter = new ValuesAdapter(session.getPerceptible().getGuidelinesList(),listener);
                break;
            case Constants.ResumeActivityPrincipes.OPERABLE:
                adapter = new ValuesAdapter(session.getOperable().getGuidelinesList(),listener);
                break;
            case Constants.ResumeActivityPrincipes.COMPRENSIBLE:
                adapter = new ValuesAdapter(session.getComprensible().getGuidelinesList(),listener);
                break;
            case Constants.ResumeActivityPrincipes.ROBUSTO:
                adapter = new ValuesAdapter(session.getRobust().getGuidelinesList(),listener);
                break;
        }

        recyclerView.setAdapter(adapter);
    }
}
