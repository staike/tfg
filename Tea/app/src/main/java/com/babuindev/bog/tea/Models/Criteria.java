package com.babuindev.bog.tea.Models;

import com.babuindev.bog.tea.Adapters.AlwaysListTypeAdapterFactory;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

public class Criteria{

    @SerializedName("numero")
    private String number;
    @SerializedName("nivel")
    private String level;
    @SerializedName("titulo")
    private String title;
    @SerializedName("tecnicas")
    @JsonAdapter(AlwaysListTypeAdapterFactory.class)
    private List<Techniques> techniquesList;

    public Criteria(String number, String level, String title, List<Techniques> techniquesList) {
        this.number = number;
        this.level = level;
        this.title = title;
        this.techniquesList = techniquesList;
    }

    public Criteria(){

    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Techniques> getTechniquesList() {
        return techniquesList;
    }

    public void setTechniquesList(Techniques ... ms) {
        techniquesList = Arrays.asList(ms);
    }
}
