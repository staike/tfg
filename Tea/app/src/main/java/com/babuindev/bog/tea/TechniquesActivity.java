package com.babuindev.bog.tea;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.babuindev.bog.tea.Adapters.TechniquesAdapter;
import com.babuindev.bog.tea.Common.Constants;
import com.babuindev.bog.tea.Common.Session;
import com.babuindev.bog.tea.Interface.TechniquesAdapterInterface;
import com.babuindev.bog.tea.Models.Criteria;
import com.babuindev.bog.tea.Models.Techniques;

import java.util.ArrayList;
import java.util.List;

public class TechniquesActivity extends AppCompatActivity {

    private Criteria criteria;
    private List<Techniques> successTechniqueList;
    private List<Techniques> errorTechniqueList;
    private List<Techniques> warningsTechniqueList;
    private List<Techniques> notAplyTechniqueList;
    private List<Techniques> defaultTechniquesList;

    private Button successButton;
    private Button errorButton;
    private Button warningButton;
    private Button notAplyButton;
    private Button defaultButton;

    private TechniquesAdapter adapter;
    private RecyclerView rv;
    private TechniquesAdapterInterface listener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_techniques);

        //We show the back button on the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getViews();

        setListeners();

        setInfo();


    }


    //Get Views
    public void getViews(){
        rv = findViewById(R.id.rv_techniques);
        successButton = findViewById(R.id.succes_techniques_button);
        errorButton = findViewById(R.id.errors_techniques_button);
        warningButton = findViewById(R.id.warnings_techniques_button);
        notAplyButton = findViewById(R.id.not_apply_techniques_button);
        defaultButton = findViewById(R.id.default_techniques_button);

    }

    //Set listeners
    public void setListeners(){
    listener = new TechniquesAdapterInterface() {
        @Override
        public void isClick(Techniques techniques) {
            //Save techniques object selected in session
            Session.getInstance(getApplicationContext()).setTechniques(techniques);
            //Start details activity
            Intent intent = new Intent(TechniquesActivity.this,DetailsActivity.class);
            startActivity(intent);
        }
    };

    successButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            adapter.setTechniquesList(successTechniqueList);
            //Set button color
            setButtonsColor(successButton);
        }
    });

    errorButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            adapter.setTechniquesList(errorTechniqueList);
            //Clear button color
            setButtonsColor(errorButton);

        }
    });

    warningButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            adapter.setTechniquesList(warningsTechniqueList);
            //Clear button color
            setButtonsColor(warningButton);
        }
    });

    notAplyButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            adapter.setTechniquesList(notAplyTechniqueList);
            //Clear button color
            setButtonsColor(notAplyButton);
        }
    });

    defaultButton.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            adapter.setTechniquesList(defaultTechniquesList);
            //Clear button color
            setButtonsColor(defaultButton);
        }
    });

    }

    //Set all info
    public void setInfo(){
        //We get the criteria object.
        criteria = Session.getInstance(getApplicationContext()).getCriteria();
        //We clasify our techniques in lists.
        clasifyTechniques();
        //Set cards in recycleview
        setCards();
        //Set title buttons in function of list size
        setTittleButtons();

    }

    public void clasifyTechniques(){
        //Instantialize lists.
        successTechniqueList = new ArrayList<>();
        errorTechniqueList = new ArrayList<>();
        warningsTechniqueList = new ArrayList<>();
        notAplyTechniqueList = new ArrayList<>();
        defaultTechniquesList = new ArrayList<>();

    if(criteria.getTechniquesList()!=null) {
        for (int i = 0; i < criteria.getTechniquesList().size(); i++) {
            //Control null techniques objects.
            if (criteria.getTechniquesList().get(i) != null) {
                //Some techniques don't have clasification so we add them to a special list to avoid null pointer exception.
                if (criteria.getTechniquesList().get(i).getClasification() != null) {

                    switch (criteria.getTechniquesList().get(i).getClasification()) {
                        case Constants.TechniquesClasificationInterface.SUCCESS:
                            successTechniqueList.add(criteria.getTechniquesList().get(i));
                            break;
                        case Constants.TechniquesClasificationInterface.ERROR:
                            errorTechniqueList.add(criteria.getTechniquesList().get(i));
                            break;
                        case Constants.TechniquesClasificationInterface.WARNING:
                            warningsTechniqueList.add(criteria.getTechniquesList().get(i));
                            break;
                        case Constants.TechniquesClasificationInterface.NOTAPLY:
                            notAplyTechniqueList.add(criteria.getTechniquesList().get(i));
                            break;
                    }

                } else {
                    //Add a clasification to techniques that has not to don't null pointer in details Activity.
                    Techniques defaultTechnique = criteria.getTechniquesList().get(i);
                    defaultTechnique.setClasification(getString(R.string.techniques_button_default));
                    defaultTechniquesList.add(defaultTechnique);
                }
            }
        }
    }
    }

    public void setCards(){
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        adapter = new TechniquesAdapter(this,successTechniqueList,listener);
        //Add layout manager and adapter to recycle view.
        rv.setLayoutManager(llm);
        rv.setAdapter(adapter);

    }

    public void setTittleButtons(){
        successButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        successButton.setText(getString(R.string.techniques_button_success)+"("+successTechniqueList.size()+")");
        errorButton.setText(getString(R.string.techniques_button_errors)+"("+errorTechniqueList.size()+")");
        warningButton.setText(getString(R.string.techniques_button_warnings)+"("+warningsTechniqueList.size()+")");
        notAplyButton.setText(getString(R.string.techniques_button_notAply)+"("+notAplyTechniqueList.size()+")");
        defaultButton.setText(getString(R.string.techniques_button_default)+"("+defaultTechniquesList.size()+")");
    }

    public void setButtonsColor(Button button){
        successButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        errorButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        warningButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        notAplyButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        defaultButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        //Set the clicked button to green.
        button.setBackgroundColor(getResources().getColor(R.color.colorAccent));

    }

}
